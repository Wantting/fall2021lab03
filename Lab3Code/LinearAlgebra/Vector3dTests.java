package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
/**
 * Vector3dTest
 * Huang wanting 2034421
 */
public class Vector3dTests {
    @Test        
    public void testGetX() {
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(1,vec.getXValue());
    }
    @Test 
    public void testGetY() {
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(2,vec.getYValue());
    }
    @Test 
    public void testGetZ() {
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(3,vec.getZValue());
    }

    @Test
    public void testMangnitude() {
        Vector3d vec = new Vector3d(3, 4, 5);
        assertEquals(Math.sqrt(50),vec.magnitude());
    }
    @Test
    public void testDotProduct() {
        Vector3d vec1 = new Vector3d(1, 1, 2);
        Vector3d vec2 = new Vector3d(2, 3, 4);
        assertEquals(13,vec1.dotProduct(vec2));
    }
    @Test
    public void testAddX() {
        Vector3d vec1 = new Vector3d(1, 2, 3);
        Vector3d vec2 = new Vector3d(2, 3, 4);
      
        assertEquals(3,vec1.add(vec2).getXValue());
    }
    @Test
    public void testAddY() {
        Vector3d vec1 = new Vector3d(1, 1, 2);
        Vector3d vec2 = new Vector3d(2, 3, 4);
      
        assertEquals(4,vec1.add(vec2).getYValue());
    }
    @Test
    public void testAddZ() {
        Vector3d vec1 = new Vector3d(1, 1, 2);
        Vector3d vec2 = new Vector3d(2, 3, 4);
       
        assertEquals(6,vec1.add(vec2).getZValue());
    }
}
