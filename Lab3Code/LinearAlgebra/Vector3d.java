/**
 * Vector3d
 * Huang wanting 2034421
 */
package LinearAlgebra;    
 public final class Vector3d {
    private final double x;
    private final double y;
    private final double z;
    
    public Vector3d(double xValue, double yValue, double zValue){
        this.x=xValue;
        this.y=yValue;
        this.z=zValue;
    }

    public double getXValue(){
        return this.x;
    }

    public double getYValue(){
        return this.y;
    }

    public double getZValue(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)+Math.pow(this.z,2));
    }
    public double dotProduct(Vector3d newVector3d){
        
        return this.x*newVector3d.getXValue()+this.y*newVector3d.getYValue()+this.z*newVector3d.getZValue();
    }
    public Vector3d add(Vector3d newVector3d){
        newVector3d = new Vector3d(this.x+newVector3d.getXValue(), this.y+newVector3d.getYValue(), this.z+newVector3d.getZValue());
        return newVector3d;
    }
}